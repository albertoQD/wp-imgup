'''
Documentation, License etc.

@package wp-imgup-kdevelop
'''
from wordpress_xmlrpc import Client, WordPressPage
from wordpress_xmlrpc.methods import posts, media
from wordpress_xmlrpc.compat import xmlrpc_client
from xml.parsers.expat import ExpatError
from wordpress_xmlrpc.exceptions import ServerConnectionError
from bs4 import BeautifulSoup
from mimetypes import guess_type
import urllib.request, urllib.error, urllib.parse
import os

WP_URL = 'http://aquintero.wordpress.com/xmlrpc.php'
WP_USR = 'aquintero'
WP_PWD = 'chino18426394'
URL_BASE = 'http://www.imdb.com'
url = '/title/tt1829012/mediaindex'
_url = '/title/tt0110912/mediaindex'
GOO_UA = 'Mozilla/5.0 (compatible; Googlebot/2.1; \
+http://www.google.com/bot.html)'


def get_html(url_address):
    # Doing the request
    request = urllib.request.Request(URL_BASE + url_address,
                              None, {'User-Agent': GOO_UA})
    res = urllib.request.urlopen(request)
    return res.read()

if __name__ == "__main__":
    # WordPress Init
    try:
        client = Client(WP_URL, WP_USR, WP_PWD)
    except ExpatError:
        print('[EE] > Not valid URL')
        exit(0)
    except ServerConnectionError as e:
        print('[EE] > Error Connecting to Server: ', e.message)
        exit(0)

    # Parsing the HTML
    soup = BeautifulSoup(get_html(_url))
    # Getting the name of the movie
    title = soup.select('h3[itemprop="name"] > a')[0].text
    print(' > Current movie: ', title)
    # Getting all the images
    more_pages = True
    page_cnt = 1

    # Create the main page for the movie in wordpress
    print(' > Creating the main page for the movie in WordPress ...')
    main_page = WordPressPage()
    main_page.title = title
    main_page.content = '<i>Still working...</i>'
    main_page.id = client.call(posts.NewPost(main_page))
    # Getting the whole info of the new page
    main_page = client.call(posts.GetPost(main_page.id))
    #print dir(main_page)
    #exit(0)
    image_pages = {}

    while more_pages:
        items = soup.select('#media_index_thumbnail_grid \
                            img[itemprop="image"]')
        img_cnt = 1
        img_len = len(items)
        for item in items:
            item_title = item.get('alt')
            item_url = item.get('src').replace('_SY100_', '_SY500_').replace(
                '0,100,100', '0,500,500').replace('_SX100_', '_SX500_')
            print(' > Downloading image ', img_cnt, '/', img_len, end=' ')
            print(' in page ', page_cnt)

            # Start downloading            
            extension = item_url[-4:]
            image_filename = title + '_' + str(img_cnt) + extension
            urllib.request.urlretrieve(item_url, image_filename)
            
            # Upload image to wordpress
            print(' > Uploading image to wordpress ...')
            filetype = guess_type(image_filename)[0]
            data = {'name': image_filename,
                    'type': filetype}
            with open(image_filename, 'rb') as img:
                data['bits'] = xmlrpc_client.Binary(img.read())
            try:
                img_info = client.call(media.UploadFile(data))
            except BrokenPipeError as e:
                print('[EE] > Error Connecting to Server: ', e.message)
                exit(0)
                    
            #--- Remove the downloaded file
            try:
                os.remove(image_filename)
            except:
                print('[WW] > Could not remove the downloaded image')
                pass

            #Create the page for the image uploaded
            print(' > Creating the page for the uploaded image ...')
            page = WordPressPage()
            page.title = item_title
            page.content = '<img alt="' + item_title + '"' + \
                ' src="' + img_info['url'] + '" />' + \
                '<p><a href="' + main_page.link + \
                '">&lt;&lt; Back to Movie Index</a></p>'
            page.post_status = 'publish'
            page.parent_id = main_page.id
            try:
                page.id = client.call(posts.NewPost(page))
            except BrokenPipeError as e:
                print('[EE] > Error Connecting to Server: ', e.message)
                exit(0)
                
            # Save for later build the main
            image_pages[page.id] = img_info
            img_cnt += 1

        # find out if there is more pages
        pagination = soup.select('.media_index_pagination .prevnext')
        if len(pagination) == 0 or pagination[0].text.find('Next') == -1:
            more_pages = False
        else:
            soup = BeautifulSoup(get_html(pagination[0].get('href')))
            page_cnt += 1

    # Create the main page HTML and update
    main_html = '<table><tr>'
    img_cnt = 1
    for page_id, img_info in image_pages.items():
        page_info = client.call(posts.GetPost(page_id))
        main_html += '<td><a href="' + page_info.link + '">' + \
                     '<img src="' + img_info['url'] + \
                     '" width="100" height="100" /></a></td>'
        if img_cnt % 5 == 0:
            main_html += '</tr><tr>'
        img_cnt += 1

    main_html += '</tr></table>'
    main_page.content = main_html
    main_page.post_status = 'publish'
    try:
        client.call(posts.EditPost(main_page.id, main_page))
    except BrokenPipeError as e:
        print('[EE] > Error Connecting to Server: ', e.message)
        exit(0)
    print(' > Main page for movie updated')
